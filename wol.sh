sudo apt update

sudo apt search vim -y

sudo apt install ethtool wakeonlan -y

ip a

echo "copiar para arquivo de texto o nome da interface e anotar o mac address, normalmente é o 2: NOMEINTERFACE / link/ether mac"

echo "
#!/bin/sh
/sbin/ethtool -s NOMEDAINTERFACE wol g
"

echo "
sudo chmod +x /etc/network/if-up.d/wol_fix.sh
"

echo "
sudo vim /etc/systemd/system/wol_fix.service
"

echo "
[Unit]
Description=Fix para que o WakeOnLAN não seja desabilitado após o Servidor ser reiniciado

[Service]
ExecStart=/etc/network/if-up.d/wol_fix.sh
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
"

echo "sudo systemctl daemon-reload
sudo systemctl enable wol_fix.service
reboot
"

echo "wakeonlan 00:00:00:00:00:00"