apt-get install gdebi --assume-yes
gdebi -n google-chrome-stable_current_amd64.deb
apt install git --assume-yes
apt install curl --assume-yes
apt install xclip --assume-yes
apt install fonts-firacode --assume-yes
snap install discord
snap install postman
snap install insomnia
snap install mailspring
snap install ora
snap install code --classic
snap install libreoffice
snap install aws-cli --classic
snap install vokoscreen-ng
snap install vlc
snap install ora
snap install sublime-text --classic
snap install spotify
snap install gimp
# snap install office20
git config --global user.name "Ismael Heinen"
git config --global user.email "ismael.heinen@gmail.com"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install yarn --assume-yes && yarn -v
apt-get update && apt install nodejs --assume-yes && apt install npm --assume-yes && nodejs -v && npm -v
yarn global add serverless@1.71.3 serverless-offline
