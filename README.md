## terminal and important
~~~
sudo apt-get install guake
sudo ln -s /usr/share/applications/guake.desktop /etc/xdg/autostart/
guake -p
sudo apt-get install gdebi
~~~
## chrome
~~~
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo gdebi google-chrome-stable_current_amd64.deb
~~~
## cerebro
~~~
wget https://github.com/KELiON/cerebro/releases/download/v0.3.1/cerebro_0.3.1_amd64.deb
sudo gdebi cerebro_0.3.1_amd64.deb
~~~
## gVim
~~~
sudo apt-get install vim-gtk3
~~~
## produtividade
~~~
sudo snap install discord
sudo snap install postman
sudo snap install insomnia
sudo snap install mailspring
sudo snap install ora
sudo snap install code --classic
sudo snap install libreoffice
sudo snap install aws-cli --classic
~~~
## ambiente
~~~
sudo apt install git
sudo apt install curl
sudo apt install xclip
sudo apt install fonts-firacode
sudo apt install exfat-fuse exfat-utils
sudo apt install python-pip
pip install -r requirements.txt
~~~
## plus
~~~
sudo apt install caffeine
adicionar no startup applications o comando /usr/bin/caffeine-indicator
~~~
### yarn
~~~
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn && yarn -v
~~~
### git ssh
~~~
ssh-keygen -t ed25519 -C "ismael.heinen@gmail.com"
xclip -sel clip < ~/.ssh/id_ed25519.pub
git config --global user.name "Ismael Heinen"
git config --global user.email "ismael.heinen@gmail.com"
~~~
colar no link: https://gitlab.com/profile/keys

### vscode - Settings Sync
~~~
71ac89f98589bd209cf750fa1af1667f
~~~
## serverless
~~~
yarn global add serverless serverless-offline
~~~
## node e npm
~~~
sudo apt-get update && sudo apt install nodejs && sudo apt install npm && nodejs -v && npm -v
~~~
## mkcert
~~~
sudo apt install wget libnss3-tools
export VER="v1.3.0"
wget -O mkcert https://github.com/FiloSottile/mkcert/releases/download/${VER}/mkcert-${VER}-linux-amd64
chmod +x  mkcert
sudo mv mkcert /usr/local/bin
~~~
### mkcert em cada projeto
~~~
mkdir .certs
cd .certs
mkcert -cert-file cert.pem -key-file key.pem -p12-file pkcs.p12 localhost 0.0.0.0 ::1
mkcert -cert-file cert.pem -key-file key.pem -p12-file pkcs.p12 -pkcs12 localhost 0.0.0.0 ::1
mkcert -install
~~~
## system monitor status bar
~~~
sudo apt-get install gnome-shell-extension-system-monitor
~~~
### aws user
~~~
aws configure
AKIA4ULFY7X2UEGXLMXU
jtGkYJ1kBLZYawrJTm3Ka9CXNKG12o1afiZgyJ2T
us-east-1
json
~~~
## qgis latest [modificar eoan pelo versão]
~~~
sudo sh -c 'echo "deb http://qgis.org/debian focal main" >> /etc/apt/sources.list'  
sudo sh -c 'echo "deb-src http://qgis.org/debian focal main " >> /etc/apt/sources.list'  
wget -O - https://qgis.org/downloads/qgis-2019.gpg.key | gpg --import
gpg --fingerprint 51F523511C7028C3
gpg --export --armor 51F523511C7028C3 | sudo apt-key add -
sudo apt-get update && sudo apt-get install qgis python-qgis
~~~
## opcionais
~~~
sudo snap install gimp
sudo snap install sublime-text --classic
sudo snap install p7zip-desktop
sudo snap install spotify
~~~
## install and run mongodb
~~~
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
sudo apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo service mongod start
sudo service mongod status
~~~
## não usuais
~~~
sudo snap install retroarch
sudo snap install blender --classic
sudo snap install winds
sudo snap install meteo
~~~

