cat ls /etc/*release /etc/*version
gedit /etc/apt/sources.list
deb     https://qgis.org/debian-ltr focal main
deb-src https://qgis.org/debian-ltr focal main
wget -O - https://qgis.org/downloads/qgis-2019.gpg.key | gpg --import
gpg --fingerprint 51F523511C7028C3
gpg --export --armor 51F523511C7028C3 | sudo apt-key add -
apt-get update
apt-get install qgis python3-qgis qgis-plugin-grass saga --assume-yes
